﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RegistrationtoDevOps_Demo.Models;


namespace RegtoDevOps_Demo
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            Registration reg = new Registration();
            reg.FirstName = "Arun";
            reg.LastName = "Gowda";
            reg.Location = "MFAR";
            reg.DOB = Convert.ToDateTime(10/12/1993);
            reg.Age = 23;
            RegtoDevOpsDemoEntities1 db = new RegtoDevOpsDemoEntities1();
            int newid= Convert.ToInt16(db.Registrations.Add(reg));
            Assert.IsTrue(newid > 0);

        }
    }
}
