﻿using RegistrationtoDevOps_Demo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;



namespace RegistrationtoDevOps_Demo.Controllers
{
    public class HomeController : Controller
    {
        //testing for bit bucket
        

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(Registration obj)

        {
            if (ModelState.IsValid)
            {
                RegtoDevOpsDemoEntities1 db = new RegtoDevOpsDemoEntities1();
                db.Registrations.Add(obj);
                db.SaveChanges();
            }
            return RedirectToAction("Result", "Home");
        }
        public ActionResult Result()
        {
            return View();
        }

    }
}